'use strict';

import middy from '@middy/core';
import jsonBodyParser from '@middy/http-json-body-parser';
import httpErrorHandler from '@middy/http-error-handler';
import httpSecurityHeaders from '@middy/http-security-headers';
import validator from '@middy/validator';

import {
  getOrder,
  PaymentResult,
  validatePaymentToken,
} from '../../functions/payments';
import { createResponse } from '../../tools/response';

const post = async (event) => {
  const { body } = event;

  const { order_id, payment_token } = body;
  const order = await getOrder(order_id);

  if (!order) {
    return createResponse({
      successCode: 500,
      errorCode: PaymentResult.INVALID,
      errorMessage: 'Unable to retrieve order',
    });
  }

  const validationResult = validatePaymentToken(order.id, payment_token);
  if (!validationResult.transaction_id) {
    return createResponse({
      successCode: 400,
      errorMessage: validationResult.error,
      error: PaymentResult.DECLINED,
    });
  }
  return createResponse({ transaction_id: validationResult.transaction_id });
};

const inputSchema = {
  required: ['body'],
  properties: {
    body: {
      order_id: {
        type: 'string',
      },
      payment_token: {
        type: 'string',
      },
    },
  },
};

export const handler = middy(post)
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(httpSecurityHeaders())
  .use(httpErrorHandler());
