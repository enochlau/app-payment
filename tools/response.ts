/* eslint-disable @typescript-eslint/no-explicit-any */
'use strict';

export interface ResponseBody {
  statusCode: number;
  body: string;
  headers: { [key: string]: any };
}

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Credentials': true,
};

export const createResponse = async (
  result: any,
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  options?: any,
): Promise<ResponseBody> => {
  const successCode = (options && options.successCode) || 200;
  try {
    return {
      statusCode: successCode,
      body: JSON.stringify(result || {}),
      headers,
    };
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify({ ok: false }),
      headers,
    };
  }
};
