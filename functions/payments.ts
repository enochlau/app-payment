const client = require('../config/environment');

export const PaymentResult = {
  APPROVED: 'approved',
  DECLINED: 'declined',
  INVALID: 'invalid',
};

export interface Order {
  id: string;
  data: OrderDetails;
  created_at: string;
  modified_at: string;
  deleted_at: string;
}

interface OrderDetails {
  details: string;
}

async function getOrder(id: string) {
  await client.connect();
  const query = `
    SELECT * FROM orders WHERE id = $1
  `;

  const result = await client.query(query, [id]);
  await client.clean();

  return result.rows[0];
}

const validatePaymentToken = (
  orderId: string,
  paymentToken: string,
): { transaction_id?: string; error?: string } => {
  // mock logic to do validation of payment, 25% chance to be declined
  const validationResult =
    Math.random() < 0.25
      ? { error: 'payment declined', orderId, paymentToken }
      : { transaction_id: orderId, orderId, paymentToken };

  // other logic and etc would be here

  return validationResult;
};

export { getOrder, validatePaymentToken };
