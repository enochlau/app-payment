import { Client } from 'pg';
import * as PaymentsFunctions from '../payments';

jest.mock('pg', () => {
  const mClient = {
    connect: jest.fn(),
    query: jest.fn(),
    end: jest.fn(),
  };
  return { Client: jest.fn(() => mClient) };
});

describe('orders functions', () => {
  let client;
  beforeEach(() => {
    client = new Client();
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('should get orders correctly', async () => {
    const data = 'testId';
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    await PaymentsFunctions.getOrder(data);
    expect(client.connect).toBeCalledTimes(1);
    expect(client.query).toHaveBeenCalledWith(
      `
    SELECT * FROM orders WHERE id = $1
  `,
      [data],
    );
    expect(client.end).toBeCalledTimes(1);
  });
  // it('should delete orders correctly', async () => {
  //   const id = 'testId';
  //   client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
  //   await PaymentsFunctions.deleteOrder(id);
  //   expect(client.connect).toBeCalledTimes(1);
  //   expect(client.query).toHaveBeenCalledWith(
  //     `
  //   UPDATE orders
  //   SET deleted_at = NOW()
  //   WHERE id = $1
  //   RETURNING *
  // `,
  //     [id],
  //   );
  //   expect(client.end).toBeCalledTimes(1);
  // });
});
